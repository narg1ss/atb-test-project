import React from 'react';
import './App.css';

class ContentForm extends React.Component {
  constructor() 
  {
    super();
    this.client = {
      data: [
        {
          from: 'Həşimzadə Ramin',
          to: 'Jumeyrah Hotel LLC',
          phone: '+994 (50) 123 45 67',
          assignment: '2 nəfərlik otaq (Kind Bed)'
        }
      ]
    }
  }

  render() {
    return (
      <div>
        {
          this.client.data.map((dynamicData, i) =>            
          <div className="content">
            <h3>ONLAYN ÖDƏNİŞ SƏHİFƏSİ</h3>
              <div className="about">
                <img src="image/receipt.png" alt="receipt"></img>
                <p>Qaimə</p>
                <div className="info">
                  <table>
                    <tr>
                        <th></th>
                        <th></th>
                    </tr>
                    <tr>
                        <td>Ödəyən</td>
                        <td class="sc">{dynamicData.from}</td>
                    </tr>
                    <tr>
                        <td>Mobil nömrə</td>
                        <td class="sc">{dynamicData.phone}</td>
                    </tr>
                  </table>
                  <hr></hr> 
                  <table>
                    <tr>
                        <td>Kimə ödənilir</td>
                        <td class="sc">{dynamicData.to}</td>
                    </tr>
                    <tr>
                        <td>Təyinat</td>
                        <td class="sc res">{dynamicData.assignment}</td>
                    </tr>
                  </table>
                </div>
              </div>
            <div class="total">
              <p class="sum"><b>Məbləğ</b></p>
              <p class="price">43.00 AZN</p>
            </div>
            <img className="union" src="image/union.png" alt="union" width='480' />
            <p className="center">ÖDƏNİŞ NÖVLƏRİ</p>
          </div>
        )
      }
      </div>
    )
  }
}

const App = ()  => {
  return (
    <div className="all body">
      <header>
      <img className="logo" src="image/logo.png" alt="Logo"></img>
      <a href="tel:945">
        <img className="phone" src="image/phone.png" alt="Phone"></img>
      </a>
      </header>
      <ContentForm />
      <footer>
        <div className="icons">
          <img src="image/visa-1.png" alt="Visa"></img>
          <img src="image/mastercard.png" alt="Maestro"></img>
          <img src="image/maestro.png" alt="Mastercard"></img>
          <img src="image/visa-2.png" style={{marginBottom: 10}} alt="Visa"></img>
        </div>
        <p>Ödə düyməsinə basaraq yuxarıda göstərilən məlumatlarla tanış olduğumu təsdiq edirəm</p>
        <a href="#"><button>ÖDƏ</button></a>
        <hr></hr>
        <p className="jsutify">© 2020 | Azər Türk Bank tərəfindən təmin edilən 3D-Secure ödəmə xidmətləri</p>
      </footer>
    </div>
  );
}

export default App;
